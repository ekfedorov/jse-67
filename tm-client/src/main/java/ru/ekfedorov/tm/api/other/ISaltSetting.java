package ru.ekfedorov.tm.api.other;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;

@Service
public interface ISaltSetting {

    @NotNull
    Integer getPasswordIteration();

    @NotNull
    String getPasswordSecret();

    @NotNull
    Integer getSignIteration();

    @NotNull
    String getSignSecret();

}
