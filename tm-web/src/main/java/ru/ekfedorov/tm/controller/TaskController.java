package ru.ekfedorov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.ekfedorov.tm.api.service.IProjectService;
import ru.ekfedorov.tm.api.service.ITaskService;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.model.Project;
import ru.ekfedorov.tm.model.Task;

import java.util.List;

@Controller
public class TaskController {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectService projectService;

    @GetMapping("/task/create")
    public String create() {
        taskService.create();
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(
            @PathVariable("id") String id
    ) {
        taskService.removeById(id);
        return "redirect:/tasks";
    }

    @ModelAttribute("statuses")
    public Status[] getStatuses() {
        return Status.values();
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(
            @PathVariable("id") String id
    ) {
        return new ModelAndView(
                "task-edit",
                "command", taskService.findById(id)
        );
    }

    @PostMapping("/task/edit/{id}")
    public String edit(
            @ModelAttribute("task") Task task
    ) {
        taskService.add(task);
        return "redirect:/tasks";
    }

    @ModelAttribute("view_name")
    public String getViewName() {
        return "TASK EDIT";
    }

    @ModelAttribute("projects")
    public List<Project> getProjects() {
        return projectService.findAll();
    }

}
