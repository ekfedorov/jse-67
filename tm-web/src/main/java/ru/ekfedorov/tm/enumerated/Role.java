package ru.ekfedorov.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum Role {

    ADMIN("administrator"),
    USER("user");

    @NotNull
    private final String displayName;

    Role(@NotNull String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}
