package ru.ekfedorov.tm.api.service;

import ru.ekfedorov.tm.model.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    void addAll(Collection<Task> collection);

    Task add(Task entity);

    void create();

    Task findById(String id);

    void clear();

    void removeById(String id);

    void remove(Task entity);

}
