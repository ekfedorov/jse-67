package ru.ekfedorov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ekfedorov.tm.repository.model.ISessionGraphRepository;
import ru.ekfedorov.tm.api.service.IPropertyService;
import ru.ekfedorov.tm.api.service.model.ISessionGraphService;
import ru.ekfedorov.tm.api.service.model.IUserGraphService;
import ru.ekfedorov.tm.enumerated.Role;
import ru.ekfedorov.tm.enumerated.Sort;
import ru.ekfedorov.tm.exception.empty.IdIsEmptyException;
import ru.ekfedorov.tm.exception.empty.LoginIsEmptyException;
import ru.ekfedorov.tm.exception.system.AccessDeniedException;
import ru.ekfedorov.tm.exception.system.NullComparatorException;
import ru.ekfedorov.tm.exception.system.NullObjectException;
import ru.ekfedorov.tm.exception.system.UserIsLockedException;
import ru.ekfedorov.tm.model.SessionGraph;
import ru.ekfedorov.tm.model.UserGraph;
import ru.ekfedorov.tm.util.HashUtil;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

@Service
public final class SessionGraphService extends AbstractGraphService<SessionGraph> implements ISessionGraphService {

    @NotNull
    @Autowired
    public ISessionGraphRepository sessionGraphRepository;

    @NotNull
    @Autowired
    private IUserGraphService userGraphService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Override
    @SneakyThrows
    public void add(@Nullable final SessionGraph session) {
        if (session == null) throw new NullObjectException();
        sessionGraphRepository.save(session);
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable List<SessionGraph> entities) {
        if (entities == null) throw new NullObjectException();
        sessionGraphRepository.saveAll(entities);
    }

    @Override
    @SneakyThrows
    public void clear() {
        sessionGraphRepository.deleteAll();
    }

    @Override
    @SneakyThrows
    public boolean contains(@NotNull final String id) {
        if (isEmpty(id)) throw new IdIsEmptyException();
        return sessionGraphRepository.existsById(id);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<SessionGraph> findAll() {
        return sessionGraphRepository.findAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<SessionGraph> findAllSort(
            @Nullable final String sort
    ) {
        if (isEmpty(sort)) throw new NullComparatorException();
        @Nullable Sort sortType = Sort.valueOf(sort);
        @NotNull final Comparator<SessionGraph> comparator = sortType.getComparator();
        return sessionGraphRepository.findAll().stream().sorted(comparator).collect(Collectors.toList());
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<SessionGraph> findOneById(
            @Nullable final String id
    ) {
        if (isEmpty(id)) throw new IdIsEmptyException();
        return Optional.of(sessionGraphRepository.getOne(id));
    }

    @SneakyThrows
    @Override
    public void removeOneById(
            @Nullable final String id
    ) {
        if (isEmpty(id)) throw new LoginIsEmptyException();
        sessionGraphRepository.deleteById(id);
    }

    @SneakyThrows
    public boolean checkDataAccess(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (isEmpty(login) || isEmpty(password)) return false;
        final @NotNull Optional<UserGraph> user = userGraphService.findByLogin(login);
        if (!user.isPresent()) return false;
        if (user.get().isLocked()) throw new UserIsLockedException();
        final String passwordHash = HashUtil.salt(propertyService, password);
        if (isEmpty(passwordHash)) return false;
        return passwordHash.equals(user.get().getPasswordHash());
    }

    @Override
    @Nullable
    @SneakyThrows
    public SessionGraph close(@Nullable final SessionGraph session) {
        if (session == null) return null;
        sessionGraphRepository.deleteById(session.getId());
        return session;
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionGraph open(
            @Nullable final String login,
            @Nullable final String password
    ) {
        final boolean check = checkDataAccess(login, password);
        if (!check) throw new AccessDeniedException();
        final @NotNull Optional<UserGraph> user = userGraphService.findByLogin(login);
        if (!user.isPresent()) return null;
        @NotNull final SessionGraph session = new SessionGraph();
        session.setUser(user.get());
        @Nullable final SessionGraph signSession = sign(session);
        if (signSession == null) return null;
        sessionGraphRepository.save(signSession);
        return signSession;
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final SessionGraph entity) {
        if (entity == null) throw new NullObjectException();
        sessionGraphRepository.deleteById(entity.getId());
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final SessionGraph session) {
        if (session == null) throw new AccessDeniedException();
        if (isEmpty(session.getSignature())) throw new AccessDeniedException();
        if (session.getUser() == null) throw new AccessDeniedException();
        @Nullable final SessionGraph temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @Nullable final SessionGraph sessionTarget = sign(temp);
        if (sessionTarget == null) throw new AccessDeniedException();
        @Nullable final String signatureTarget = sessionTarget.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        if (!sessionGraphRepository.existsById(session.getId())) throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    public void validateAdmin(@Nullable final SessionGraph session, @Nullable final Role role) {
        if (session == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        if (isEmpty(session.getUser().getId())) throw new AccessDeniedException();
        validate(session);
        final @NotNull Optional<UserGraph> user = userGraphService.findOneById(session.getUser().getId());
        if (!user.isPresent()) throw new AccessDeniedException();
        if (user.get().getRole() != Role.ADMIN) throw new AccessDeniedException();
    }

    @Nullable
    public SessionGraph sign(@Nullable final SessionGraph session) {
        if (session == null) return null;
        session.setSignature(null);
        @Nullable final String signature = HashUtil.salt(propertyService, session);
        session.setSignature(signature);
        return session;
    }

}
