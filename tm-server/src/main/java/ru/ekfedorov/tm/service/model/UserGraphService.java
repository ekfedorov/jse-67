package ru.ekfedorov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ekfedorov.tm.repository.model.IUserGraphRepository;
import ru.ekfedorov.tm.api.service.IPropertyService;
import ru.ekfedorov.tm.api.service.model.IUserGraphService;
import ru.ekfedorov.tm.enumerated.Role;
import ru.ekfedorov.tm.enumerated.Sort;
import ru.ekfedorov.tm.exception.empty.*;
import ru.ekfedorov.tm.exception.system.LoginExistException;
import ru.ekfedorov.tm.exception.system.NullComparatorException;
import ru.ekfedorov.tm.exception.system.NullObjectException;
import ru.ekfedorov.tm.model.UserGraph;
import ru.ekfedorov.tm.util.HashUtil;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

@Service
public final class UserGraphService extends AbstractGraphService<UserGraph> implements IUserGraphService {

    @NotNull
    @Autowired
    public IUserGraphRepository userGraphRepository;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Override
    @SneakyThrows
    @Transactional
    public void add(@Nullable final UserGraph user) {
        if (user == null) throw new NullObjectException();
        userGraphRepository.save(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void addAll(@Nullable List<UserGraph> entities) {
        if (entities == null) throw new NullObjectException();
        userGraphRepository.saveAll(entities);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        userGraphRepository.deleteAll();
    }

    @Override
    @SneakyThrows
    public boolean contains(@NotNull final String id) {
        if (isEmpty(id)) throw new IdIsEmptyException();
        return userGraphRepository.existsById(id);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<UserGraph> findAll() {
        return userGraphRepository.findAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<UserGraph> findAllSort(
            @Nullable final String sort
    ) {
        if (isEmpty(sort)) throw new NullComparatorException();
        @Nullable Sort sortType = Sort.valueOf(sort);
        @NotNull final Comparator<UserGraph> comparator = sortType.getComparator();
        return userGraphRepository.findAll().stream().sorted(comparator).collect(Collectors.toList());
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<UserGraph> findOneById(
            @Nullable final String id
    ) {
        if (isEmpty(id)) throw new IdIsEmptyException();
        return Optional.of(userGraphRepository.getOne(id));
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneById(
            @Nullable final String id
    ) {
        if (isEmpty(id)) throw new LoginIsEmptyException();
        userGraphRepository.deleteById(id);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void create(
            @Nullable final String login, @Nullable final String password
    ) {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        if (isLoginExist(login)) throw new LoginExistException();
        if (isEmpty(password)) throw new PasswordIsEmptyException();
        @NotNull final UserGraph user = new UserGraph();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
    }

    @SneakyThrows
    @Override
    @Transactional
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        if (isLoginExist(login)) throw new LoginExistException();
        if (isEmpty(password)) throw new PasswordIsEmptyException();
        if (isEmpty(email)) throw new EmailIsEmptyException();
        @NotNull final UserGraph user = new UserGraph();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        add(user);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        if (isLoginExist(login)) throw new LoginExistException();
        if (isEmpty(password)) throw new PasswordIsEmptyException();
        if (role == null) throw new RoleIsEmptyException();
        @NotNull final UserGraph user = new UserGraph();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        add(user);
    }

    @SneakyThrows
    @NotNull
    @Override
    public Optional<UserGraph> findByLogin(
            @Nullable final String login
    ) {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        return userGraphRepository.findByLogin(login);
    }

    @SneakyThrows
    @Override
    public boolean isLoginExist(
            @Nullable final String login
    ) {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        return userGraphRepository.findByLogin(login).isPresent();
    }

    @SneakyThrows
    @Override
    @Transactional
    public void lockUserByLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        @NotNull final Optional<UserGraph> userOptional = findByLogin(login);
        if (!userOptional.isPresent()) throw new UserNotFoundException();
        @NotNull final UserGraph user = userOptional.get();
        user.setLocked(true);
        userGraphRepository.save(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(@Nullable final UserGraph entity) {
        if (entity == null) throw new NullObjectException();
        userGraphRepository.deleteById(entity.getId());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeByLogin(
            @Nullable final String login
    ) {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        userGraphRepository.removeByLogin(login);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void setPassword(
            @Nullable final String userId, @Nullable final String password
    ) {
        if (isEmpty(userId)) throw new IdIsEmptyException();
        if (isEmpty(password)) throw new PasswordIsEmptyException();
        @NotNull final Optional<UserGraph> user = findOneById(userId);
        if (!user.isPresent()) return;
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) return;
        user.get().setPasswordHash(hash);
        userGraphRepository.save(user.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void unlockUserByLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        @NotNull final Optional<UserGraph> userOptional = findByLogin(login);
        if (!userOptional.isPresent()) throw new UserNotFoundException();
        @NotNull final UserGraph user = userOptional.get();
        user.setLocked(false);
        userGraphRepository.save(user);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void updateUser(
            @Nullable final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (isEmpty(userId)) throw new IdIsEmptyException();
        @NotNull final Optional<UserGraph> user = findOneById(userId);
        if (!user.isPresent()) throw new NullObjectException();
        user.get().setFirstName(firstName);
        user.get().setLastName(lastName);
        user.get().setMiddleName(middleName);
        userGraphRepository.save(user.get());
    }

}
