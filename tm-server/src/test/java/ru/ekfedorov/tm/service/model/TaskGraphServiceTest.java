package ru.ekfedorov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.ekfedorov.tm.api.service.IConnectionService;
import ru.ekfedorov.tm.api.service.model.IProjectGraphService;
import ru.ekfedorov.tm.api.service.model.ITaskGraphService;
import ru.ekfedorov.tm.api.service.model.IUserGraphService;
import ru.ekfedorov.tm.cofiguration.ServerConfiguration;
import ru.ekfedorov.tm.marker.DBCategory;
import ru.ekfedorov.tm.model.TaskGraph;
import ru.ekfedorov.tm.model.UserGraph;
import ru.ekfedorov.tm.service.TestUtil;

import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TaskGraphServiceTest {

    @NotNull AnnotationConfigApplicationContext context =
            new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @NotNull
    private final IUserGraphService userService = context.getBean(IUserGraphService.class);

    @NotNull
    private final ITaskGraphService taskService = context.getBean(ITaskGraphService.class);

    {
        TestUtil.initUser();
    }

    @Before
    public void before() {
        context.getBean(EntityManagerFactory.class).createEntityManager();
    }

    @After
    public void after() {
        context.getBean(EntityManagerFactory.class).close();
    }

    @Test
    @Category(DBCategory.class)
    public void addAllTest() {
        final List<TaskGraph> tasks = new ArrayList<>();
        final TaskGraph task1 = new TaskGraph();
        final TaskGraph task2 = new TaskGraph();
        tasks.add(task1);
        tasks.add(task2);
        taskService.addAll(tasks);
        Assert.assertTrue(taskService.findOneById(task1.getId()).isPresent());
        Assert.assertTrue(taskService.findOneById(task2.getId()).isPresent());
        taskService.remove(tasks.get(0));
        taskService.remove(tasks.get(1));
    }

    @Test
    @Category(DBCategory.class)
    public void addTest() {
        final TaskGraph task = new TaskGraph();
        taskService.add(task);
        Assert.assertNotNull(taskService.findOneById(task.getId()));
        taskService.remove(task);
    }

    @Test
    @Category(DBCategory.class)
    public void clearTest() {
        taskService.clear();
        Assert.assertTrue(taskService.findAll().isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void containsTest() {
        final TaskGraph task = new TaskGraph();
        final String taskId = task.getId();
        taskService.add(task);
        Assert.assertTrue(taskService.contains(taskId));
        taskService.remove(task);
    }

    @Test
    @Category(DBCategory.class)
    public void findAll() {
        final int taskSize = taskService.findAll().size();
        final List<TaskGraph> tasks = new ArrayList<>();
        final TaskGraph task1 = new TaskGraph();
        final TaskGraph task2 = new TaskGraph();
        tasks.add(task1);
        tasks.add(task2);
        taskService.addAll(tasks);
        Assert.assertEquals(2 + taskSize, taskService.findAll().size());
        taskService.remove(task1);
        taskService.remove(task2);
    }

    @Test
    @Category(DBCategory.class)
    public void findAllSortByUserId() {
        taskService.clear();
        final List<TaskGraph> tasks = new ArrayList<>();
        final TaskGraph task1 = new TaskGraph();
        final TaskGraph task2 = new TaskGraph();
        final TaskGraph task3 = new TaskGraph();
        final @NotNull Optional<UserGraph> user = userService.findByLogin("test");
        final String userId = user.get().getId();
        task1.setName("b");
        task2.setName("c");
        task3.setName("a");
        task1.setUser(user.get());
        task2.setUser(user.get());
        task3.setUser(user.get());
        tasks.add(task1);
        tasks.add(task2);
        tasks.add(task3);
        taskService.addAll(tasks);
        final String sort = "NAME";
        final List<TaskGraph> tasks2 = new ArrayList<>(taskService.findAll(userId, sort));
        Assert.assertFalse(tasks2.isEmpty());
        Assert.assertEquals(3, tasks2.size());
        Assert.assertEquals("a", tasks2.get(0).getName());
        Assert.assertEquals("b", tasks2.get(1).getName());
        Assert.assertEquals("c", tasks2.get(2).getName());
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIdTest() {
        final TaskGraph task = new TaskGraph();
        final String taskId = task.getId();
        taskService.add(task);
        Assert.assertNotNull(taskService.findOneById(taskId));
        taskService.remove(task);
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIndexTest() {
        final TaskGraph task = new TaskGraph();
        taskService.add(task);
        final String taskId = task.getId();
        Assert.assertTrue(taskService.findOneById(taskId).isPresent());
        taskService.remove(task);
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIndexTestByUserId() {
        final TaskGraph task = new TaskGraph();
        final @NotNull Optional<UserGraph> user = userService.findByLogin("test");
        final String userId = user.get().getId();
        task.setUser(user.get());
        taskService.add(task);
        final String taskId = task.getId();
        Assert.assertTrue(taskService.findOneById(userId, taskId).isPresent());
        taskService.remove(task);
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByNameTest() {
        final TaskGraph task = new TaskGraph();
        final @NotNull Optional<UserGraph> user = userService.findByLogin("test");
        final String userId = user.get().getId();
        task.setUser(user.get());
        task.setName("pr1");
        taskService.add(task);
        final String name = task.getName();
        Assert.assertNotNull(name);
        Assert.assertTrue(taskService.findOneByName(userId, name).isPresent());
        taskService.remove(task);
    }

    @Test
    @Category(DBCategory.class)
    public void removeOneByIdTest() {
        final TaskGraph task = new TaskGraph();
        taskService.add(task);
        final String taskId = task.getId();
        taskService.removeOneById(taskId);
        Assert.assertFalse(taskService.findOneById(taskId).isPresent());
    }

    @Test
    @Category(DBCategory.class)
    public void removeOneByIdTestByUserId() {
        final TaskGraph task = new TaskGraph();
        final @NotNull Optional<UserGraph> user = userService.findByLogin("test");
        final String userId = user.get().getId();
        task.setUser(user.get());
        taskService.add(task);
        final String taskId = task.getId();
        taskService.removeOneById(userId, taskId);
        Assert.assertFalse(taskService.findOneById(taskId).isPresent());
    }

    @Test
    @Category(DBCategory.class)
    public void removeOneByIndexTest() {
        final TaskGraph task1 = new TaskGraph();
        final TaskGraph task2 = new TaskGraph();
        final TaskGraph task3 = new TaskGraph();
        final @NotNull Optional<UserGraph> user = userService.findByLogin("test");
        final String userId = user.get().getId();
        task1.setUser(user.get());
        task2.setUser(user.get());
        task3.setUser(user.get());
        taskService.add(task1);
        taskService.add(task2);
        taskService.add(task3);
        Assert.assertTrue(taskService.findOneByIndex(userId, 0).isPresent());
        Assert.assertTrue(taskService.findOneByIndex(userId, 1).isPresent());
        Assert.assertTrue(taskService.findOneByIndex(userId, 2).isPresent());
    }

    @Test
    @Category(DBCategory.class)
    public void removeOneByNameTest() {
        final TaskGraph task = new TaskGraph();
        final @NotNull Optional<UserGraph> user = userService.findByLogin("test");
        final String userId = user.get().getId();
        task.setUser(user.get());
        task.setName("pr1");
        taskService.add(task);
        final String name = task.getName();
        Assert.assertNotNull(name);
        taskService.removeOneByName(userId, name);
        Assert.assertFalse(taskService.findOneByName(userId, name).isPresent());
    }

    @Test
    @Category(DBCategory.class)
    public void removeTest() {
        final TaskGraph task = new TaskGraph();
        taskService.add(task);
        taskService.remove(task);
        Assert.assertNotNull(taskService.findOneById(task.getId()));
    }

    @Test
    @Category(DBCategory.class)
    public void removeTestByUserIdAndObject() {
        final TaskGraph task = new TaskGraph();
        final @NotNull Optional<UserGraph> user = userService.findByLogin("test");
        final String userId = user.get().getId();
        task.setUser(user.get());
        taskService.add(task);
        taskService.remove(userId, task);
        Assert.assertFalse(taskService.findOneById(task.getId()).isPresent());
    }

}
