package ru.ekfedorov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;
import ru.ekfedorov.tm.api.service.*;
import ru.ekfedorov.tm.api.service.dto.IProjectService;
import ru.ekfedorov.tm.api.service.dto.ITaskService;
import ru.ekfedorov.tm.api.service.dto.IUserService;
import ru.ekfedorov.tm.cofiguration.ServerConfiguration;
import ru.ekfedorov.tm.dto.Project;
import ru.ekfedorov.tm.dto.User;
import ru.ekfedorov.tm.endpoint.AdminEndpoint;
import ru.ekfedorov.tm.endpoint.SessionEndpoint;
import ru.ekfedorov.tm.marker.DBCategory;
import ru.ekfedorov.tm.service.TestUtil;

import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ProjectServiceTest {

    @NotNull AnnotationConfigApplicationContext context =
            new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @NotNull
    private final IProjectService projectService = context.getBean(IProjectService.class);

    @NotNull
    private final IUserService userService = context.getBean(IUserService.class);

    @NotNull
    private final ITaskService taskService = context.getBean(ITaskService.class);

    {
        TestUtil.initUser();
    }

    @Before
    public void before() {
        context.getBean(EntityManagerFactory.class).createEntityManager();
    }

    @After
    public void after() {
        context.getBean(EntityManagerFactory.class).close();
    }

    @Test
    @Category(DBCategory.class)
    public void addAllTest() {
        final List<Project> projects = new ArrayList<>();
        final Project project1 = new Project();
        final Project project2 = new Project();
        projects.add(project1);
        projects.add(project2);
        projectService.addAll(projects);
        Assert.assertTrue(projectService.findOneById(project1.getId()).isPresent());
        Assert.assertTrue(projectService.findOneById(project2.getId()).isPresent());
        projectService.remove(projects.get(0));
        projectService.remove(projects.get(1));
    }

    @Test
    @Category(DBCategory.class)
    public void addTest() {
        final Project project = new Project();
        projectService.add(project);
        Assert.assertNotNull(projectService.findOneById(project.getId()));
        projectService.remove(project);
    }

    @Test
    @Category(DBCategory.class)
    public void clearTest() {
        taskService.clear();
        projectService.clear();
        Assert.assertTrue(projectService.findAll().isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void containsTest() {
        final Project project = new Project();
        final String projectId = project.getId();
        projectService.add(project);
        Assert.assertTrue(projectService.contains(projectId));
        projectService.remove(project);
    }

    @Test
    @Category(DBCategory.class)
    public void findAll() {
        final int projectSize = projectService.findAll().size();
        final List<Project> projects = new ArrayList<>();
        final Project project1 = new Project();
        final Project project2 = new Project();
        projects.add(project1);
        projects.add(project2);
        projectService.addAll(projects);
        Assert.assertEquals(2 + projectSize, projectService.findAll().size());
        projectService.remove(project1);
        projectService.remove(project2);
    }

    @Test
    @Category(DBCategory.class)
    public void findAllSortByUserId() {
        taskService.clear();
        projectService.clear();
        final List<Project> projects = new ArrayList<>();
        final Project project1 = new Project();
        final Project project2 = new Project();
        final Project project3 = new Project();
        final @NotNull Optional<User> user = userService.findByLogin("test");
        final String userId = user.get().getId();
        project1.setName("b");
        project2.setName("c");
        project3.setName("a");
        project1.setUserId(userId);
        project2.setUserId(userId);
        project3.setUserId(userId);
        projects.add(project1);
        projects.add(project2);
        projects.add(project3);
        projectService.addAll(projects);
        final String sort = "NAME";
        final List<Project> projects2 = new ArrayList<>(projectService.findAll(userId, sort));
        Assert.assertFalse(projects2.isEmpty());
        Assert.assertEquals(3, projects2.size());
        Assert.assertEquals("a", projects2.get(0).getName());
        Assert.assertEquals("b", projects2.get(1).getName());
        Assert.assertEquals("c", projects2.get(2).getName());
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIdTest() {
        final Project project = new Project();
        final String projectId = project.getId();
        projectService.add(project);
        Assert.assertNotNull(projectService.findOneById(projectId));
        projectService.remove(project);
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIndexTest() {
        final Project project = new Project();
        projectService.add(project);
        final String projectId = project.getId();
        Assert.assertTrue(projectService.findOneById(projectId).isPresent());
        projectService.remove(project);
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIndexTestByUserId() {
        final Project project = new Project();
        final @NotNull Optional<User> user = userService.findByLogin("test");
        final String userId = user.get().getId();
        project.setUserId(userId);
        projectService.add(project);
        final String projectId = project.getId();
        Assert.assertTrue(projectService.findOneById(userId, projectId).isPresent());
        projectService.remove(project);
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByNameTest() {
        final Project project = new Project();
        final @NotNull Optional<User> user = userService.findByLogin("test");
        final String userId = user.get().getId();
        project.setUserId(userId);
        project.setName("pr1");
        projectService.add(project);
        final String name = project.getName();
        Assert.assertNotNull(name);
        Assert.assertTrue(projectService.findOneByName(userId, name).isPresent());
        projectService.remove(project);
    }

    @Test
    @Category(DBCategory.class)
    public void removeOneByIdTest() {
        final Project project = new Project();
        projectService.add(project);
        final String projectId = project.getId();
        projectService.removeOneById(projectId);
        Assert.assertFalse(projectService.findOneById(projectId).isPresent());
    }


    @Test
    @Category(DBCategory.class)
    public void removeOneByIdTestByUserId() {
        final Project project = new Project();
        final @NotNull Optional<User> user = userService.findByLogin("test");
        final String userId = user.get().getId();
        project.setUserId(userId);
        projectService.add(project);
        final String projectId = project.getId();
        projectService.removeOneById(userId, projectId);
        Assert.assertFalse(projectService.findOneById(userId, projectId).isPresent());
    }

    @Test
    @Category(DBCategory.class)
    public void removeOneByIndexTest() {
        final Project project1 = new Project();
        final Project project2 = new Project();
        final Project project3 = new Project();
        final @NotNull Optional<User> user = userService.findByLogin("test");
        final String userId = user.get().getId();
        project1.setUserId(userId);
        project2.setUserId(userId);
        project3.setUserId(userId);
        projectService.add(project1);
        projectService.add(project2);
        projectService.add(project3);
        Assert.assertTrue(projectService.findOneByIndex(userId, 0).isPresent());
        Assert.assertTrue(projectService.findOneByIndex(userId, 1).isPresent());
        Assert.assertTrue(projectService.findOneByIndex(userId, 2).isPresent());
    }

    @Test
    @Category(DBCategory.class)
    public void removeOneByNameTest() {
        final Project project = new Project();
        final @NotNull Optional<User> user = userService.findByLogin("test");
        final String userId = user.get().getId();
        project.setUserId(userId);
        project.setName("pr1");
        projectService.add(project);
        final String name = project.getName();
        Assert.assertNotNull(name);
        projectService.removeOneByName(userId, name);
        Assert.assertFalse(projectService.findOneByName(userId, name).isPresent());
    }

    @Test
    @Category(DBCategory.class)
    public void removeTest() {
        final Project project = new Project();
        projectService.add(project);
        projectService.remove(project);
        Assert.assertNotNull(projectService.findOneById(project.getId()));
    }

    @Test
    @Category(DBCategory.class)
    public void removeTestByUserIdAndObject() {
        final Project project = new Project();
        final @NotNull Optional<User> user = userService.findByLogin("test");
        final String userId = user.get().getId();
        project.setUserId(userId);
        projectService.add(project);
        projectService.remove(userId, project);
        Assert.assertFalse(projectService.findOneById(userId, project.getId()).isPresent());
    }

}
